'''
Created on Sep 10, 2017

@author: Pooja Singhal
'''
import falcon
import MySQLdb
import json
import datetime
import sys
import time
from FetchFiles import FetchFiles

class UploadFiles:
    
    
    def on_post(self,req,res):
        try:
            data = req.stream.read()
        except Exception as e:
            raise falcon.HTTPError(falcon.HTTP_400,
                'Error',
                e.message)
        try:
            data =json.loads(data)
            file_name = str("'" + data.get("file_name")+"'")
            timestamp = time.time()
            user_id=  int(data.get("user_id"))
            db = MySQLdb.connect(host="localhost",user="root",password='root123', database='adplatform')
            cur = db.cursor()    
            cur.execute("Insert into files_upload (user_id,file_name,timestamp) values (" +user_id+",'"+file_name+"','"+timestamp+"')")
            cur.commit()
            cur.close()
            
        except Exception as e:
            raise falcon.HTTPError(falcon.HTTP_400,
                                   'Malformed JSON',
                                   'Could not decode the request body. The JSON was incorrect.')
            resp.status = falcon.HTTP_202
            
        resp.body = json.dumps("{status: OK, message : File Inserted successfully}", encoding='utf-8')
        api = falcon.API()
        api.add_route('/files', FetchFiles())
  
        
        